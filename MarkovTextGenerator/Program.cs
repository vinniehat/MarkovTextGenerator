﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MarkovTextGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Chain chain = new Chain();

            Console.WriteLine("Welcome to Vinniehat's Markov Text Generator!");
            Console.WriteLine("	\t\n      .___.\n     /     \\\n    | O _ O |\n    /  \\_/  \\ \n  .' /     \\ `.\n / _|       |_ \\\n(_/ |       | \\_)\n    \\       /\n   __\\_>-<_/__\n   ~;/     \\;~");

            Console.WriteLine("To use this program, enter a sentence! After each sentence, hit enter. Once finished, type \"!\" on a new line!");

            LoadText("Sample.txt", chain);

            while (true)
            {

                Console.Write("> ");

                String line = Console.ReadLine();
                if (line == "!")
                    break;

                chain.AddString(line);  // Let the chain process this string
            }

            // Now let's update all the probabilities with the new data
            chain.UpdateProbabilities();

            // Okay now for the fun part
            Console.WriteLine("Done learning!  Now give me a word and I'll tell you what comes next.");
            Console.Write("> ");

            String word = Console.ReadLine();
            String nextWord = chain.GetNextWord(word);
            String sentence = chain.GenerateSentence();
            String beginning = $"{word} {nextWord}";
            Console.WriteLine("Would you like to sentence(1) or paragraph(2)? ");
            var option = Console.ReadLine();
            if (option == "1")
            {
                Console.WriteLine("Generating Sentence... ");
                for (int i=0; i <= 100; i++)
                {
                    Console.Write("\r{0}%", i);
                    Thread.Sleep(20);
                }
                Console.WriteLine("\nHere is your sentence: " + beginning + sentence);
            } else if (option == "2")
            {
                Console.WriteLine("Generating Paragraph... ");
                for (int i=0; i <= 100; i++)
                {
                    Console.Write("\r{0}%", i);
                    Thread.Sleep(40);
                }
                String s1 = chain.GenerateSentence();
                String s1w = chain.GetNextWord(word);
                String s2 = chain.GenerateSentence();
                String s2w = chain.GetNextWord(word);
                String s3 = chain.GenerateSentence();
                String s3w = chain.GetNextWord(word);
                String s4 = chain.GenerateSentence();
                String s4w = chain.GetNextWord(word);
                String s5 = chain.GenerateSentence();
                String s5w = chain.GetNextWord(word);
                String paragraph = $"{word} {s1w}{s1}. {word} {s2w}{s2}. {word} {s3w}{s3}. {word} {s4w}{s4}. {word} {s5w}{s5}.";
                Console.WriteLine("\nHere is your paragraph: ");
                Console.WriteLine("------------------------------");
                Console.WriteLine(paragraph);
            }
            else
            {
                Console.WriteLine("YOU ENTERED THE WRONG NUMBER!");
                Console.WriteLine("https://images.vinniehat.com/video/1");
                Console.WriteLine("https://images.vinniehat.com/video/1");
                Console.WriteLine("https://images.vinniehat.com/video/1");
                Console.WriteLine("https://images.vinniehat.com/video/1");
            }
           
        }

        static void LoadText(string filename, Chain chain)
        {
            int counter = 0;
            String line;

            string path = Path.Combine(Environment.CurrentDirectory, $"data\\{filename}");
            StreamReader file = new StreamReader(path);
            while ((line = file.ReadLine()) != null)
            {
                chain.AddString(line);
                counter++;
            }

            file.Close();
        }
    }
}
