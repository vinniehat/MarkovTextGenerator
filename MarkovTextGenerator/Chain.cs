﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkovTextGenerator
{
    public class Chain
    {
        public Dictionary<String, List<Word>> words;
        private Dictionary<String, int> sums;
        private Random rand;
        private List<String> StartingWords;

        public Chain()
        {
            words = new Dictionary<String, List<Word>>();
            sums = new Dictionary<string, int>();
            rand = new Random(System.Environment.TickCount);
            StartingWords = new List<String>();
        }

        /// <summary>
        /// Returns a random starting word from the stored list of words
        /// This may not be the best approach.. better may be to actually store
        /// a separate list of actual sentence starting words and randomly choose from that
        /// </summary>
        /// <returns></returns>
        public String GetRandomStartingWord()
        {
            return StartingWords.ElementAt(rand.Next() % StartingWords.Count);
        }

        // Adds a sentence to the chain
        // You can use the empty string to indicate the sentence will end
        //
        // For example, if sentence is "The house is on fire" you would do the following:
        //  AddPair("The", "house")
        //  AddPair("house", "is")
        //  AddPair("is", "on")
        //  AddPair("on", "fire")
        //  AddPair("fire", "")

        public void AddString(String sentence)
        {
            var newSentence = sentence.Split(' ');

            for (int i = 0; i < newSentence.Length; i++)
            {
                if(i == 0) StartingWords.Add(newSentence[i]);
                if ((i + 1) < newSentence.Length) AddPair(newSentence[i], newSentence[i + 1]);
                else AddPair(newSentence[i], "");
            }
        }

        // Adds a pair of words to the chain that will appear in order
        public void AddPair(String word, String word2)
        {
            if (!words.ContainsKey(word))
            {
                sums.Add(word, 1);
                words.Add(word, new List<Word>());
                words[word].Add(new Word(word2));
            }
            else
            {
                bool found = false;
                foreach (Word s in words[word])
                {
                    if (s.ToString() == word2)
                    {
                        found = true;
                        s.Count++;
                        sums[word]++;
                    }
                }

                if (!found)
                {
                    words[word].Add(new Word(word2));
                    sums[word]++;
                }
            }
        }

        /// <summary>
        /// Given a word, randomly chooses the next word.  This should be done
        /// by using the list of words in the words Dictionary.  The provided
        /// code allows you to pick a word from the choices array.  Bear in mind
        /// that each word is not equally likely to occur and has their own probability
        /// of occurring.
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public String GetNextWord(String word)
        {
            if (words.ContainsKey(word))
            {
                List<Word> choices = words[word];
                Double r = rand.NextDouble();
                Double total = 0;
                foreach (var choice in choices)
                {
                    total += choice.Probability;
                    if (r < total) return choice.ToString();
                }
            }

            return "No Word Found";
        }

        /// <summary>
        /// Generates a full randomly generated sentence based that starts with
        /// startingWord.
        /// </summary>
        /// <returns></returns>
        public String GenerateSentence()
        {
            string sentence = "";
            String previousWord = GetRandomStartingWord();
            while (true)
            {
                String word = GetNextWord(previousWord);
                previousWord = word;
                if (word == "") break;
                sentence = sentence + " " + word;
            }
            return sentence;
        }

        /// <summary>
        /// Updates the probability of choosing a second word at random
        /// for a chain of words attached to a first word.
        /// Example: If the starting word is "The" and the only word that
        /// you ever see following it is the word "cat", then "cat" would
        /// have a probability of following "The" of 1.0.  Another scenario
        /// would involve sentences like:
        /// The cat loves milk, The cat is my friend, The dog is in the yard
        /// In this scenario with the starting word of "The":
        /// - cat would have a probability of 0.66 (appears 66% of the time)
        /// - dog would have a probability of 0.33 (appears 33% of the time)
        /// </summary>
        public void UpdateProbabilities()
        {
            foreach (String word in words.Keys)
            {
                double sum = 0; // Total sum of all the occurrences of each followup word

                // Update the probabilities
                foreach (Word s in words[word])
                {
                    s.Probability = (double) s.Count / sums[word];
                }
            }
        }
    }
}